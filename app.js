//TODO init enviado cada frame, separar Bullet de Player, arreglar initpack, deletepack
//en cliente hacer que el server solo envie la data necesaria ( score)
//no enviar data si no esta logueado
let mongojs = require('mongojs');
let db = mongojs("mongodb+srv://admin:admin@cluster0.s3jtr.mongodb.net/pruebitajuego?retryWrites=true&w=majority", ['account']);
let express = require('express');
let app = express();
let serv = require('http').Server(app)

app.get('/', function (req, res) {
    res.sendFile(__dirname + '/client/index.html');
});
app.use('/client', express.static(__dirname + '/client'));
serv.listen(8080);
console.log('Server started');
let SOCKET_LIST = {};



const {Player, Bullet} = require("./server/Player");
let DEBUG = true;


let validarContra = function(data,cb){
    db.account.find({username:data.username,password:data.password},function(err,res){
        if(res.length > 0)
            cb(true);
        else
            cb(false);
    });
}
let isUsernameTaken = function(data,cb){
    db.account.find({username:data.username},function(err,res){
        if(res.length > 0)
            cb(true);
        else
            cb(false);
    });
}
let addUser = function(data,cb){
    db.account.insert({username:data.username,password:data.password},function(){
        cb();
    });
}

let io = require('socket.io')(serv, {});
io.sockets.on('connection', function (socket) {
    SOCKET_LIST[socket.id] = socket;
    socket.on('signIn',function(data){
        validarContra(data,function(res){
            if(res){    
                Player.onConnect(socket);
                socket.emit('signInResponse',{success:true});
            }else{
                socket.emit('signInResponse',{success:false});
            }
        });
       
    });
    socket.on('signUp',function(data){
        isUsernameTaken(data,function(res){
            if(res){  
                socket.emit('signUpResponse',{success:false});
            }else{
                addUser(data,function(){
                    socket.emit('signUpResponse',{success:true,username:data.username});
                });
                
            }
        });

        
    });
    
    socket.on('disconnect', function () {
        delete SOCKET_LIST[socket.id];
        Player.onDisconnect(socket);

    });
    socket.on('sendMsg', function (data) {
        let playerName = ("" + socket.id).slice(2, 7)
        for (let i in SOCKET_LIST) {
            SOCKET_LIST[i].emit('addToChat', playerName + ': ' + data)
        }
    });
    socket.on('eval', function (data) {
        if (!DEBUG)
            return;
        let res = eval(data);
        socket.emit('evalAnswer', res);
    });
});
let initPack={player:[],bullet:[]};
let removePack ={player:[],bullet:[]};
Player.setInitPack(initPack);

Player.setRemovePack(removePack);
setInterval(function () {
    let pack = {
        player: Player.update(),
        bullet: Bullet.update()
    }

    for (let i in SOCKET_LIST) {
        let socket = SOCKET_LIST[i];
        socket.emit('init', initPack);
        socket.emit('update', pack);
        socket.emit('remove',removePack);
    }
    initPack.player = [];
    initPack.bullet = [];
    removePack.player = [];
    removePack.bullet = [];
}, 1000 / 25);
