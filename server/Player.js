const Entity = require("./Entity");
let Player = function (id) {
    let self = Entity();
    self.id = id;
    self.number = "" + Math.floor(10 * Math.random());
    self.pressingRight = false;
    self.pressingLeft = false;
    self.pressingUp = false;
    self.pressingDown = false;
    self.pressingAttack = false;
    self.shotAngle = 0;
    self.maxSpd = 10;
    self.hp = 10;
    self.hpMax = 10;
    self.score = 0;

    let super_update = self.update;
    self.update = function () {
        self.updateSpd();
        super_update();
        if (self.pressingAttack) {
            self.shotBullet(self.shotAngle);
        }
    }
    self.shotBullet = function (angle) {
        let b = Bullet(self.id, angle);
        b.x = self.x;
        b.y = self.y;
    }
    self.updateSpd = function () {
        if (self.pressingRight)
            self.spdX = self.maxSpd;
        else if (self.pressingLeft)
            self.spdX = -self.maxSpd;
        else
            self.spdX = 0;

        if (self.pressingUp)
            self.spdY = -self.maxSpd;
        else if (self.pressingDown)
            self.spdY = self.maxSpd;
        else
            self.spdY = 0;
    }
    Player.list[id] = self;

    self.getInitPack = function(){
        return {
            id:self.id,
            x:self.x,
            y:self.y,  
            number:self.number,
            hp:self.hp,
            hpMax:self.hpMax,
            score:self.score, 
        };
    }
    self.getUpdatePack= function(){
        return {
            id:self.id,
            x:self.x,
            y:self.y,
            hp:self.hp,
            score:self.score,   
        };
    }
    initPack.player.push(self.getInitPack());
    return self;
}
Player.list = {};
Player.onConnect = function (socket) {
    let player = Player(socket.id);
    socket.on('keypress', function (data) {
        if (data.inputId === 'left')
            player.pressingLeft = data.state;
        else if (data.inputId === 'right')
            player.pressingRight = data.state;
        else if (data.inputId === 'up')
            player.pressingUp = data.state;
        else if (data.inputId === 'down')
            player.pressingDown = data.state;
        else if (data.inputId === 'attack')
            player.pressingAttack = data.state;
        else if (data.inputId === 'mouseAngle')
            player.shotAngle = data.state;
    });
   
    //initPack.selfId = socket.id;
    
    socket.emit('init',{
        selfId:socket.id,
        player:Player.getAllInitPack(),
        bullet:Bullet.getAllInitPack()
    })
}
Player.getAllInitPack = function(){
    let players =[];
    for(let i in Player.list){
       // console.log(Player.list[i].getInitPack());
        players.push(Player.list[i].getInitPack());
    }
    return players;
}
Player.onDisconnect = function (socket) {
    delete Player.list[socket.id];
    removePack.player.push(socket.id);
}
Player.update = function () {
    let pack = [];
    for (let i in Player.list) {
        let player = Player.list[i];
        player.update();
        pack.push(player.getUpdatePack());
    }
    return pack;
}
let initPack={player:[],bullet:[]};
let removePack ={player:[],bullet:[]};
Player.setInitPack = function(_initPack){
    initPack = _initPack;
}
Player.setRemovePack = function(_removePack){
    removePack = _removePack;
}

let Bullet = function (parent, angle) {
    let self = Entity();
    self.id = Math.random();
    self.spdX = Math.cos(angle / 180 * Math.PI) * 10;
    self.spdY = Math.sin(angle / 180 * Math.PI) * 10;
    self.parent = parent;
    self.timer = 0;
    self.toRemove = false;
    let super_update = self.update;
    self.update = function () {
        if (self.timer++ > 100)
            self.toRemove = true;
        super_update();
        
        for (let i in Player.list) {
       
            let p = Player.list[i];
    
            if (self.getDistance(p) < 32 && self.parent !== p.id) {
                p.hp -= 1;

                if (p.hp <= 0) {
                    let shooter = Player.list[self.parent];
                    if (shooter) {
                        shooter.score += 1;
                    }
                    p.hp = p.hpMax;
                    p.x = Math.random() * 500;
                    p.y = Math.random() * 500;
                }
                self.toRemove = true;
            }
        }
    }
    Bullet.list[self.id] = self;
    self.getInitPack = function () {
        return {
            id: self.id,
            x: self.x,
            y: self.y,
        };
    }
    self.getUpdatePack = function () {
        return {
            id: self.id,
            x: self.x,
            y: self.y,
        };
    }
    initPack.bullet.push(self.getInitPack());
    return self;
}
Bullet.list = {};
Bullet.getAllInitPack = function () {
    let bullets = [];
    for (let i in Bullet.list) {
        bullets.push(Bullet.list[i].getInitPack());
    }
    return bullets;
}
Bullet.update = function () {
    let pack = [];
    for (let i in Bullet.list) {
        let bullet = Bullet.list[i];
        bullet.update();
        if (bullet.toRemove === true) {
            delete Bullet.list[i];
            removePack.bullet.push(bullet.id);
        } else {
            pack.push(bullet.getUpdatePack());
        }
    }
    return pack;
}
module.exports = {Player , Bullet};