const Entity = require("./Entity");
const Player = require("./Player");

let Bullet = function (parent, angle) {
    let self = Entity();
    self.id = Math.random();
    self.spdX = Math.cos(angle / 180 * Math.PI) * 10;
    self.spdY = Math.sin(angle / 180 * Math.PI) * 10;
    self.parent = parent;
    self.timer = 0;
    self.toRemove = false;
    let super_update = self.update;
    self.update = function () {
        if (self.timer++ > 100)
            self.toRemove = true;
        super_update();
        
        for (let i in Player.list) {
            console.log(Player.list);
            let p = Player.list[i];
            console.log(p.number);
            if (self.getDistance(p) < 32 && self.parent !== p.id) {
                p.hp -= 1;
                console.log("colision");
                if (p.hp <= 0) {
                    let shooter = Player.list[self.parent];
                    if (shooter) {
                        shooter.score += 1;
                    }
                    p.hp = p.hpMax;
                    p.x = Math.random() * 500;
                    p.y = Math.random() * 500;
                }
                self.toRemove = true;
            }
        }
    }
    Bullet.list[self.id] = self;
    self.getInitPack = function () {
        return {
            id: self.id,
            x: self.x,
            y: self.y,
        };
    }
    self.getUpdatePack = function () {
        return {
            id: self.id,
            x: self.x,
            y: self.y,
        };
    }
    initPack.bullet.push(self.getInitPack());
    return self;
}
Bullet.list = {};
Bullet.getAllInitPack = function () {
    let bullets = [];
    for (let i in Bullet.list) {
        bullets.push(Bullet.list[i].getInitPack());
    }
    return bullets;
}
Bullet.update = function () {
    let pack = [];
    for (let i in Bullet.list) {
        let bullet = Bullet.list[i];
        bullet.update();
        if (bullet.toRemove === true) {
            delete Bullet.list[i];
            removePack.bullet.push(bullet.id);
        } else {
            pack.push(bullet.getUpdatePack());
        }
    }
    return pack;
}
let initPack = { player: [], bullet: [] };
let removePack = { player: [], bullet: [] };
Bullet.setInitPack = function (_initPack) {
    initPack = _initPack;
}
Bullet.setRemovePack = function (_removePack) {
    removePack = _removePack;
}
module.exports = Bullet;

